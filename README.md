# Setperset

A 7x7 font created in the "Fonts.txt" workshop at the Free Culture Forum 2014,
facilitated by Manufactura Independente. It was designed and built in 4 hours.

Read more about the workshop at the [Type:Bits site](https://typebits.gitlab.io).

![Font preview](https://gitlab.com/manufacturaind/mfi-setperset/-/jobs/artifacts/master/raw/MFI-Setperset-Regular.png?job=build-font)
  
Download formats:

* [Zip file with all formats](https://gitlab.com/manufacturaind//-/jobs/artifacts/master/raw/MFI-Setperset.zip?job=build-font)
* [TrueType (.ttf)](https://gitlab.com/manufacturaind//-/jobs/artifacts/master/raw/MFI-Setperset-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/manufacturaind/font-setperset/-/jobs/artifacts/master/raw/MFI-Setperset-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/manufacturaind/font-setperset/-/jobs/artifacts/master/raw/MFI-Setperset-Regular.sfd?job=build-font)

## Authors

* Ana Isabel Carvalho
* Ricardo Lafuente
* Nelson Dieguez, [@nelsondieguez](https://twitter.com/nelsondieguez)
* Oriol Gayán
* Vanessa Pacheco
* Irene Serrano

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
