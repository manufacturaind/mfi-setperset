FONT_NAME = MFI-Setperset
BITMAPFONTBUILDER_SCRIPT = ../bitmapfontbuilder/bmfb.py

default: build

# simple local build
build:
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Regular.json
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Bold.json
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Thin.json
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Round-Regular.json
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Round-Bold.json
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Round-Thin.json
	mv generated/* . -f
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Regular.sfd --ttf
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Bold.sfd --ttf
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Thin.sfd --ttf
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Round-Regular.sfd --ttf
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Round-Bold.sfd --ttf
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Round-Thin.sfd --ttf
	fontimage $(FONT_NAME)-Regular.ttf
	fontimage $(FONT_NAME)-Bold.ttf
	fontimage $(FONT_NAME)-Thin.ttf
	fontimage $(FONT_NAME)-Round-Regular.ttf
	fontimage $(FONT_NAME)-Round-Bold.ttf
	fontimage $(FONT_NAME)-Round-Thin.ttf
	zip $(FONT_NAME).zip *.ttf *.png LICENSE README.md

# for use within gitlab's CI environment
ci:
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Regular.json
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Bold.json
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Thin.json
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Round-Regular.json
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Round-Bold.json
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Round-Thin.json
	mv generated/* . -f
	fib convert $(FONT_NAME)-Regular.sfd --ttf
	fib convert $(FONT_NAME)-Bold.sfd --ttf
	fib convert $(FONT_NAME)-Thin.sfd --ttf
	fib convert $(FONT_NAME)-Round-Regular.sfd --ttf
	fib convert $(FONT_NAME)-Round-Bold.sfd --ttf
	fib convert $(FONT_NAME)-Round-Thin.sfd --ttf
	fontimage $(FONT_NAME)-Regular.ttf
	fontimage $(FONT_NAME)-Bold.ttf
	fontimage $(FONT_NAME)-Thin.ttf
	fontimage $(FONT_NAME)-Round-Regular.ttf
	fontimage $(FONT_NAME)-Round-Bold.ttf
	fontimage $(FONT_NAME)-Round-Thin.ttf
	zip $(FONT_NAME).zip *.ttf *.png *.otf LICENSE README.md

install:
	virtualenv .env --system-site-packages
	. `pwd`/.env/bin/activate; pip install git+https://gitlab.com/foundry-in-a-box/fib.git

clean:
	rm -f *.ttf *.sfd *.png *.otf *.zip
